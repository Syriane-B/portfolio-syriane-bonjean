//dépendances
import '../css/style.scss';

// ajout de la class currentNav sur la liste de nav (menu background)
for (const li of document.querySelectorAll('div.nav > ul > li')) {
    li.addEventListener('mouseover', function () {
        li.classList.add('currentNav');
    });
    li.addEventListener('mouseout', function () {
        li.classList.remove('currentNav');
    });
}
//Ajout de la classe currentNav sur le menu en fonction du scroll
window.addEventListener('scroll', function () {
    let navCVClassList = document.getElementById('navCV').classList;
    let navPortfolioClassList = document.getElementById('navPortfolio').classList;
    let navContactClassList = document.getElementById('navContact').classList;
    //si la fenetre est dans les coordonées de cv bloc mettre le style current nav sur navCV
    if (window.pageYOffset >= document.getElementById('cv').offsetTop
        && window.pageYOffset < document.getElementById('sect-portfolio').offsetTop) {
        navCVClassList.add('currentNav');
        navPortfolioClassList.remove('currentNav');
        navContactClassList.remove('currentNav');
    }
    //si la fenetre est dans les coordonées de sect-portfolio mettre le style current nav sur portfolio
    else if (window.pageYOffset >= document.getElementById('sect-portfolio').offsetTop
        && window.pageYOffset < document.getElementById('bloc-form').offsetTop) {
        navCVClassList.remove('currentNav');
        navPortfolioClassList.add('currentNav');
        navContactClassList.remove('currentNav');
    }
    //si la fenetre est dans les coordonées de bloc-form mettre le style current nav sur contact
    else if (window.pageYOffset >= document.getElementById('bloc-form').offsetTop) {
        navCVClassList.remove('currentNav');
        navPortfolioClassList.remove('currentNav');
        navContactClassList.add('currentNav');
    }
    //sinon ne rien mettre en curentNav
    else {
        navCVClassList.remove('currentNav');
        navPortfolioClassList.remove('currentNav');
        navContactClassList.remove('currentNav');
    }
});

if (navigator.userAgent.match(/(android|iphone|blackberry|symbian|symbianos|symbos|netfront|model-orange|javaplatform|iemobile|windows phone|samsung|htc|opera mobile|opera mobi|opera mini|presto|huawei|blazer|bolt|doris|fennec|gobrowser|iris|maemo browser|mib|cldc|minimo|semc-browser|skyfire|teashark|teleca|uzard|uzardweb|meego|nokia|bb10|playbook)/gi)) {
    //Mobile ou tablette  = disable horizontal scroll
    document.getElementById('emptyscroll').remove();
    document.getElementById('bloc-mini-prj').classList.add('mobile')
} else {
    //desktop
    //horizontal scroll in mini-prj section
    let blocMiniPrj = document.getElementById('bloc-mini-prj').offsetTop;
    let blocForm = document.getElementById('bloc-form').offsetTop;
    let blocMiniPrjHeight = document.getElementById('bloc-mini-prj').offsetHeight;
    let scrollHeight = blocForm - blocMiniPrj - blocMiniPrjHeight;
    window.addEventListener('scroll', function () {
        //when the page Offset top is above the mini prj bloc
        if (window.pageYOffset < blocMiniPrj) {
            document.getElementById('bloc-mini-prj').classList.remove('fixed');
            document.getElementById('mini-prj-container').style.transform = "translateX(0)";
        }
            //Horizontal scroll
        //when the page Offset top is below the bloc to horizontal scrolling and the page Offset bottom is above the form bloc
        else if (window.pageYOffset >= blocMiniPrj
            && window.pageYOffset <= (blocForm - blocMiniPrjHeight)) {
            //fix the bloc to scroll, set the top = 0
            document.getElementById('bloc-mini-prj').classList.add('fixed');
            document.getElementById('bloc-mini-prj').style.top = "0";
            //add margin to the form bloc and start translating X on scroll
            document.getElementById('emptyscroll').style.marginBottom = "99vh";
            document.getElementById('mini-prj-container').style.transform = `translateX(${-(window.pageYOffset - blocMiniPrj) / scrollHeight * 100 / 3 * 2}%)`;
        }
            //Vertical scroll at the end
        //When the bottom of the bloc is between form bloc and the top
        else if (window.pageYOffset > (blocForm - blocMiniPrjHeight)
            && window.pageYOffset < blocForm) {
            //make sure that the scroll bloc is fixed
            document.getElementById('bloc-mini-prj').classList.add('fixed');
            //fix horizontal position
            document.getElementById('mini-prj-container').style.transform = "translateX(-66.412%)";
            //horizontal scroll
            document.getElementById('bloc-mini-prj').style.top = `${-(window.pageYOffset - blocForm + blocMiniPrjHeight) / blocMiniPrjHeight * 100}vh`;
        }
            //
        //When page Offset top is in form bloc
        else {
            //make sure that the scroll bloc is fixed
            document.getElementById('bloc-mini-prj').classList.add('fixed');
            //fixed the horizontal scroll
            document.getElementById('bloc-mini-prj').style.top = "-100vh";
            //fix horizontal position
            document.getElementById('mini-prj-container').style.transform = "translateX(-66.412%)";
            //make sure the empty scroll still have its margin bottom
            document.getElementById('emptyscroll').style.marginBottom = "99vh";
        }
    });
}

//form front cheking
let $form = document.getElementById('bloc-contact');
$form.addEventListener('submit', function (event){
    event.preventDefault();
    let surname = document.getElementById('surname').value;
    let name = document.getElementById('name').value;
    let mail = document.getElementById('mail').value;
    let phone = document.getElementById('phone').value;
    let msg = document.getElementById('msg').value;
    if (surname !== '' && name !== '' && mail !== '' && phone !== '' && msg !== '') {

        $form.submit();
    }
});

//ScrollToTop
// Scroll To Top
function scrollToTop() {
    window.scrollTo({top: 0, behavior: 'smooth'});
}
document.getElementById('toTopButton').addEventListener('click', scrollToTop);